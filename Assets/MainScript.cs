
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class MainScript : MonoBehaviour,IPointerClickHandler
{
    [SerializeField] List<GameObject> items=new List<GameObject>();
    [SerializeField] RectTransform parent;
   
    public void OnPointerClick(PointerEventData eventData)
    {
        if (items.Count > 0)
        {
            int i = Random.Range(0, items.Count);
            GameObject Item = Instantiate(items[i], Input.mousePosition, Quaternion.identity);
            Item.transform.SetParent(parent);
            items.Remove(items[i]);
        }

    }
}
